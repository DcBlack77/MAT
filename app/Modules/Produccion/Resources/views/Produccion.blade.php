@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Produccion']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Produccion.',
        'columnas' => [
		'Productor' => '6.6666666666667',
		'Rubros' => '6.6666666666667',
		'Ente Crediticio' => '6.6666666666667',
        'fecha cosecha' => '6.6666666666667',
		'Fecha Siembra' => '6.6666666666667',
		'Fecha arrime' => '6.6666666666667',
		'agroindustria' => '6.6666666666667',
		'Het Def' => '6.6666666666667',
		'Estimacion' => '6.6666666666667',
		'Produccion Kg' => '6.6666666666667',
		'N Estimacion' => '6.6666666666667',
		'Produccion Kg y ton' => '6.6666666666667',
		'rendimiento' => '6.6666666666667',
		'arrime' =>     '6.66666666666667',
		'silo' =>     '6.66666666666667',
		'numero de guía' =>     '6.66666666666667',
		'estado' =>     '6.66666666666667',
		'Tec Resp' => '6.6666666666667',
		'Total Produccion' => '6.6666666666667'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Produccion->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection