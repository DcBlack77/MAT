@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Tecnico Responsable']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar tecnico.',
        'columnas' => [
            'Nombre' => '50',
		'Documento De Identidad' => '50'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Reprezodi->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection