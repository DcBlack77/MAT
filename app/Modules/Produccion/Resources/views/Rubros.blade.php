@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Rubros']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Rubros.',
        'columnas' => [
            'Nombre' => '50',
		'Tipo' => '50',
        'variedad' => '50'
          ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Rubros->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection