@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')

    @include('base::partials.ubicacion', ['ubicacion' => ['Productor']])

    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Productor.',
        'columnas' => [
            'cedula' => '14.285714285714',
    		'Nombre Y  apellidos' => '14.285714285714',
    		'Telefono' => '14.285714285714',
    		'Municipios' => '14.285714285714',
    		'Parroquias' => '14.285714285714',
    		'Sector' => '14.285714285714',
    		'Unidad Prod' => '14.285714285714'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Productor->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection
