<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Produccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produccion', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamp('fecha_cosecha')->nullable();
			$table->timestamp('fecha_arrime')->nullable();
			$table->timestamp('fecha_siembra')->nullable();
			$table->integer('productor_id')->unsigned();
			$table->integer('rubros_id')->unsigned();
			$table->integer('estados_id')->unsigned();
			$table->string('ente_crediticio', 250);
			$table->string('rendimiento', 250);
			$table->string('het_def', 250);
			$table->string('estimacion', 250);
			$table->string('produccion_kg', 250);
			$table->string('silo', 250);
			$table->string('knsa', 250);
			$table->string('kna', 250);
			$table->string('condicion',250);
			$table->string('tec_resp', 250);
			$table->string('arrime_silo', 250);
			$table->string('N_deplanilla',250);
			$table->string('numero_guia',250);
			$table->string('fundo_pilones_mercados',250);
			
			$table->string('total_produccion', 250);
			$table->string('arrime_agroindustria', 250);
            $table->foreign('productor_id')
				->references('id')->on('productor')
				->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('rubros_id')
				->references('id')->on('rubros')
				->onDelete('cascade')->onUpdate('cascade');

			$table->timestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produccion');
    }
}
