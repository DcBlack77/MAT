<?php

Route::group(['middleware' => 'web', 'prefix' => Config::get('admin.prefix').'/produccion', 'namespace' => 'App\\Modules\Produccion\Http\Controllers'], function()
{

    Route::get('/',                 'ProduccionController@index');
    Route::get('nuevo',             'ProduccionController@nuevo');
    Route::get('cambiar/{id}',      'ProduccionController@cambiar');

    Route::get('buscar/{id}',       'ProduccionController@buscar');

    Route::post('guardar',          'ProduccionController@guardar');
    Route::put('guardar/{id}',      'ProduccionController@guardar');

    Route::delete('eliminar/{id}',  'ProduccionController@eliminar');
    Route::post('restaurar/{id}',   'ProduccionController@restaurar');
    Route::delete('destruir/{id}',  'ProduccionController@destruir');

    Route::get('datatable',         'ProduccionController@datatable');


    Route::group(['prefix' => 'productor'], function() {
        Route::get('/',                 'ProductorController@index');
        Route::get('nuevo',             'ProductorController@nuevo');
        Route::get('cambiar/{id}',      'ProductorController@cambiar');

        Route::get('buscar/{id}',       'ProductorController@buscar');

        Route::post('guardar',          'ProductorController@guardar');
        Route::put('guardar/{id}',      'ProductorController@guardar');

        Route::delete('eliminar/{id}',  'ProductorController@eliminar');
        Route::post('restaurar/{id}',   'ProductorController@restaurar');
        Route::delete('destruir/{id}',  'ProductorController@destruir');

        Route::get('datatable',         'ProductorController@datatable');
        Route::get('parroquia/{id}',    'ProductorController@parroquia');
    });


    Route::group(['prefix' => 'rubros'], function() {
        Route::get('/',                 'RubrosController@index');
        Route::get('nuevo',             'RubrosController@nuevo');
        Route::get('cambiar/{id}',      'RubrosController@cambiar');

        Route::get('buscar/{id}',       'RubrosController@buscar');

        Route::post('guardar',          'RubrosController@guardar');
        Route::put('guardar/{id}',      'RubrosController@guardar');

        Route::delete('eliminar/{id}',  'RubrosController@eliminar');
        Route::post('restaurar/{id}',   'RubrosController@restaurar');
        Route::delete('destruir/{id}',  'RubrosController@destruir');

        Route::get('datatable',         'RubrosController@datatable');
    });


    Route::group(['prefix' => 'reprezodi'], function() {
        Route::get('/',                 'ReprezodiController@index');
        Route::get('nuevo',             'ReprezodiController@nuevo');
        Route::get('cambiar/{id}',      'ReprezodiController@cambiar');

        Route::get('buscar/{id}',       'ReprezodiController@buscar');

        Route::post('guardar',          'ReprezodiController@guardar');
        Route::put('guardar/{id}',      'ReprezodiController@guardar');

        Route::delete('eliminar/{id}',  'ReprezodiController@eliminar');
        Route::post('restaurar/{id}',   'ReprezodiController@restaurar');
        Route::delete('destruir/{id}',  'ReprezodiController@destruir');

        Route::get('datatable',         'ReprezodiController@datatable');
    });


    Route::group(['prefix' => 'transporte'], function() {
        Route::get('/',                 'TransporteController@index');
        Route::get('nuevo',             'TransporteController@nuevo');
        Route::get('cambiar/{id}',      'TransporteController@cambiar');

        Route::get('buscar/{id}',       'TransporteController@buscar');

        Route::post('guardar',          'TransporteController@guardar');
        Route::put('guardar/{id}',      'TransporteController@guardar');

        Route::delete('eliminar/{id}',  'TransporteController@eliminar');
        Route::post('restaurar/{id}',   'TransporteController@restaurar');
        Route::delete('destruir/{id}',  'TransporteController@destruir');

        Route::get('datatable',         'TransporteController@datatable');
    });

    //{{route}}
});
