<?php

namespace App\Modules\Produccion\Http\Requests;

use App\Http\Requests\Request;

class ProductorRequest extends Request {
    protected $reglasArr = [
		'cedula' => ['required', 'min:3', 'max:250'],
		'nombre' => ['required', 'min:3', 'max:250'],
		'telefono' => ['required', 'min:3', 'max:250'],
		'municipios_id' => ['required', 'integer'],
		'parroquias_id' => ['required', 'integer'],
		'sector' => ['required', 'min:3', 'max:250'],
		'unidad_prod' => ['required']
	];
}
