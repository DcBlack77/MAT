<?php

namespace App\Modules\Produccion\Http\Controllers;

//Controlador Padre
use App\Modules\Produccion\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Produccion\Http\Requests\RubrosRequest;

//Modelos
use App\Modules\Produccion\Models\Rubros;

class RubrosController extends Controller
{
    protected $titulo = 'Rubros';

    public $js = [
        'Rubros'
    ];
    
    public $css = [
        'Rubros'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('produccion::Rubros', [
            'Rubros' => new Rubros()
        ]);
    }

    public function nuevo()
    {
        $Rubros = new Rubros();
        return $this->view('produccion::Rubros', [
            'layouts' => 'base::layouts.popup',
            'Rubros' => $Rubros
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Rubros = Rubros::find($id);
        return $this->view('produccion::Rubros', [
            'layouts' => 'base::layouts.popup',
            'Rubros' => $Rubros
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Rubros = Rubros::withTrashed()->find($id);
        } else {
            $Rubros = Rubros::find($id);
        }

        if ($Rubros) {
            return array_merge($Rubros->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(RubrosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Rubros = $id == 0 ? new Rubros() : Rubros::find($id);

            $Rubros->fill($request->all());
            $Rubros->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Rubros->id,
            'texto' => $Rubros->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Rubros::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Rubros::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Rubros::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Rubros::select([
            'id', 'nombre', 'tipo','variedad', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}