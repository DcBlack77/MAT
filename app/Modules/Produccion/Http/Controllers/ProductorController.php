<?php

namespace App\Modules\Produccion\Http\Controllers;

//Controlador Padre
use App\Modules\Produccion\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Produccion\Http\Requests\ProductorRequest;

//Modelos
use App\Modules\Produccion\Models\Productor;
use App\Modules\Base\Models\Municipio;
use App\Modules\Base\Models\Parroquia;


class ProductorController extends Controller
{
    protected $titulo = 'Productor';

    public $js = [
        'Productor'
    ];

    public $css = [
        'Productor'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('produccion::Productor', [
            'Productor' => new Productor()
        ]);
    }
    public function municipio()
    {
        return Municipio::where('estados_id', 6)->pluck('nombre', 'id');

    }
    public function parroquia(Request $request, $id = 0)
    {
       $Parroquias = Parroquia::where('municipios_id', $id)->pluck('nombre', 'id');
      //dd($Parroquias);
       return ['s' => 's', 'msj' => 'parroquias encontradas', 'parroquias_id' => $Parroquias];

    }

    public function nuevo()
    {
        $Productor = new Productor();
        return $this->view('produccion::Productor', [
            'layouts' => 'base::layouts.popup',
            'Productor' => $Productor
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Productor = Productor::find($id);
        return $this->view('produccion::Productor', [
            'layouts' => 'base::layouts.popup',
            'Productor' => $Productor
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Productor = Productor::withTrashed()->find($id);
        } else {
            $Productor = Productor::find($id);
        }

        if ($Productor) {
            return array_merge($Productor->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }


    public function guardar(ProductorRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Productor = $id == 0 ? new Productor() : Productor::find($id);

            $Productor->fill($request->all());
            $Productor->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Productor->id,
            'texto' => $Productor->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Productor::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Productor::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Productor::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }



    public function datatable(Request $request)
    {
        $sql = Productor::select([
            'id', 'cedula', 'nombre', 'telefono', 'municipios_id', 'parroquias_id', 'sector', 'unidad_prod', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}
