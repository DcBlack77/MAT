<?php

namespace App\Modules\Produccion\Http\Controllers;

//Controlador Padre
use App\Modules\Produccion\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Produccion\Http\Requests\ProduccionRequest;

//Modelos
use App\Modules\Produccion\Models\Produccion;

class ProduccionController extends Controller
{
    protected $titulo = 'Produccion';

    public $js = [
        'Produccion'
    ];

    public $css = [
        'Produccion'
    ];

    public $librerias = [
        'jquery-ui',
        'jquery-ui-timepicker',
        'datatables'
    ];

    public function index()
    {
        return $this->view('produccion::Produccion', [
            'Produccion' => new Produccion()
        ]);
    }

    public function nuevo()
    {
        $Produccion = new Produccion();
        return $this->view('produccion::Produccion', [
            'layouts' => 'base::layouts.popup',
            'Produccion' => $Produccion
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Produccion = Produccion::find($id);
        return $this->view('produccion::Produccion', [
            'layouts' => 'base::layouts.popup',
            'Produccion' => $Produccion
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Produccion = Produccion::withTrashed()->find($id);
        } else {
            $Produccion = Produccion::find($id);
        }

        if ($Produccion) {
            return array_merge($Produccion->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ProduccionRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Produccion = $id == 0 ? new Produccion() : Produccion::find($id);

            $Produccion->fill($request->all());
            $Produccion->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Produccion->id,
            'texto' => $Produccion->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Produccion::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Produccion::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Produccion::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Produccion::select([
            'id', 
            'fecha_siembra',
            'fecha_cosecha',
            'fecha_arrime',
            'productor_id', 
            'rubros_id',
            'ente_crediticio',
            'het_def', 
            'estimacion', 
            'produccion_kg',
            'redimiento',
            'arrime_silo',
            'silo',
            'knsa',
            'kna',
            'condicion',
            'N_deplanilla',
            'numero_guia',
            'estados_id',
            'tec_resp', 
            'total_produccion',
            'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}
