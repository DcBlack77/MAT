<?php

namespace App\Modules\Produccion\Http\Controllers;

//Controlador Padre
use App\Modules\Produccion\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Produccion\Http\Requests\TransporteRequest;

//Modelos
use App\Modules\Produccion\Models\Transporte;

class TransporteController extends Controller
{
    protected $titulo = 'Transporte';

    public $js = [
        'Transporte'
    ];
    
    public $css = [
        'Transporte'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('produccion::Transporte', [
            'Transporte' => new Transporte()
        ]);
    }

    public function nuevo()
    {
        $Transporte = new Transporte();
        return $this->view('produccion::Transporte', [
            'layouts' => 'base::layouts.popup',
            'Transporte' => $Transporte
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Transporte = Transporte::find($id);
        return $this->view('produccion::Transporte', [
            'layouts' => 'base::layouts.popup',
            'Transporte' => $Transporte
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Transporte = Transporte::withTrashed()->find($id);
        } else {
            $Transporte = Transporte::find($id);
        }

        if ($Transporte) {
            return array_merge($Transporte->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(TransporteRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Transporte = $id == 0 ? new Transporte() : Transporte::find($id);

            $Transporte->fill($request->all());
            $Transporte->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Transporte->id,
            'texto' => $Transporte->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Transporte::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Transporte::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Transporte::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Transporte::select([
            'id', 'arrime_cvg', 'arrime_silo', 'numero_guia', 'guia_fuera', 'arrime_agroindustria', 'fundo_pilones_mercados', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}