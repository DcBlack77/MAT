<?php

namespace App\Modules\Produccion\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\base\Models\Modelo;



class Transporte extends Modelo
{
    protected $table = 'transporte';
    protected $fillable = ["arrime_cvg",];
    protected $campos = [
    'arrime_cvg' => [
        'type' => 'text',
        'label' => 'Arrime Cvg',
        'placeholder' => 'Arrime Cvg del Transporte'
    ],
    
    
    
    
   
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
         
    }


    public function produccion()
    {
        return $this->hasMany('App\ModulesProduccion\Models\Produccion');
    }


}
