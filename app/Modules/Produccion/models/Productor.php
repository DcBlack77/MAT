<?php

namespace App\Modules\Produccion\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Models\Modelo;

use App\Modules\Base\Models\Municipio;
use App\Modules\Base\Models\Parroquia;


class Productor extends Modelo
{
    protected $table = 'productor';
    protected $fillable = ["cedula","nombre","telefono","municipios_id","parroquias_id","sector","unidad_prod"];
    protected $campos = [
    'cedula' => [
        'type' => 'text',
        'label' => 'Cedula o RIF',
        'placeholder' => 'Cedula o RIF del Productor'
    ],
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Productor'
    ],
    'telefono' => [
        'type' => 'text',
        'label' => 'Telefono',
        'placeholder' => 'Telefono del Productor'
    ],
    'municipios_id' => [
        'type' => 'select',
        'label' => 'Municipios',
        'placeholder' => '- Seleccione un Municipios',
        'url' => 'municipios'
    ],
    'parroquias_id' => [
        'type' => 'select',
        'label' => 'Parroquias',
        'placeholder' => '- Seleccione un Parroquias',
        'url' => 'parroquias'
    ],
    'sector' => [
        'type' => 'text',
        'label' => 'Sectores',
        'placeholder' => '- Escriba un Sector'
    ],
    'unidad_prod' => [
        'type' => 'text',
        'label' => 'Unidad Prod',
        'placeholder' => 'Unidad Prod del Productor'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['municipios_id']['options'] = Municipio::where('estados_id', '=', 6)->pluck('nombre', 'id');
		/*$this->campos['parroquias_id']['options'] = Parroquia::where('municipios_id', $id)->pluck('nombre','id');*/
    }

    public function municipio()
        {
            return $this->belongsTo('App\Modules\Base\Models\Municipio', 'municipios_id');
        }
    public function parroquia()
        {
            return $this->belongsTo('App\Modules\Base\Models\Parroquia', 'municipio_id');
        }

    public function produccion()
    {
        return $this->hasMany('App\ModulesProduccion\Models\Produccion');
    }


}
